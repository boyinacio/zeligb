package jmetal.algorithm.ga;

import java.io.IOException;
import java.util.Comparator;

import jmetal.operators.selection.SingleObjectiveMinimumRouletteWheelSelection;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.comparator.ObjectiveComparator;

import weka.core.DistanceFunction;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zeligB.util.assigner.ClusterNumberAssigner;
import zeligB.util.clusteringEvaluators.AdjustedRandIndex;
import zeligB.util.clusteringEvaluators.BasicDaviesBouldin;
import zeligB.util.clusteringEvaluators.MXIndex;
import zeligB.util.distances.JoaoCarlosDistance;

public class GeneticKMeansAlgorithm_Main {

	private Algorithm<DefaultIntegerSolution> algorithm;
	private Instances dataset;
	private DistanceFunction distance;

	public void loadDataset(String filename) {
		try {
			DataSource source = new DataSource(filename);
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void loadAlgorithm() {
		loadGKA();
	}

	public void loadGKA() {
		int numInstances = dataset.numInstances();

		int n = 50;
		int mAX_GEN = 100;

		double pm = 0.05;
		int numGrupos = dataset.numClasses();

		Comparator<IntegerSolution> comparator = new ObjectiveComparator<IntegerSolution>(
				0);

		this.distance = new JoaoCarlosDistance();

		SingleObjectiveMinimumRouletteWheelSelection<DefaultIntegerSolution> selection = new SingleObjectiveMinimumRouletteWheelSelection<DefaultIntegerSolution>();

		this.algorithm = new GeneticKMeansAlgorithm<DefaultIntegerSolution>(
				numGrupos, pm, n, mAX_GEN, dataset, numInstances, distance,
				comparator, selection);
	}

	public static void main(String[] args) {
		GeneticKMeansAlgorithm_Main app = new GeneticKMeansAlgorithm_Main();
		app.run();
	}

	public void run() {
		loadDataset("/opt/weka-3-6-12/data/iris.arff");
		loadAlgorithm();
		algorithm.run();

		IntegerSolution solution = algorithm.getResult();

		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();

		Instances newDataset = assigner.assign(
				(DefaultIntegerSolution) solution, new Instances(dataset));

		System.out.println("Base carrregada: " + dataset.relationName());
		System.out.println("Índices");
		System.out.println("------------------------");

		try {
			System.out.println("DB: " + BasicDaviesBouldin.main(newDataset));
			System.out.println("RAND: "
					+ AdjustedRandIndex.ARI(dataset, newDataset));
			System.out.println("MX: "
					+ MXIndex.evaluate(newDataset, distance, 0.8));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
