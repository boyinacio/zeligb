package jmetal.algorithm.ga;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import jmetal.operators.mutation.GKAMutation;
import jmetal.operators.mutation.KMeansOperator;
import jmetal.problem.clustering.GKAProblem;

import org.uma.jmetal.algorithm.impl.AbstractGeneticAlgorithm;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import weka.core.DistanceFunction;
import weka.core.Instances;
import zeligB.util.Mathematics;
import zeligB.util.assigner.ClusterNumberAssigner;

public class GeneticKMeansAlgorithm<S extends Solution<?>> extends
		AbstractGeneticAlgorithm<S, S> {

	private static final long serialVersionUID = 8726582293798728184L;

	private GKAProblem problem;
	private KMeansOperator kmeansOperator;
	private GKAMutation mutationOperator;
	private Instances dataset;
	private DistanceFunction distance;
	
	private int N; // population size
	private int geno, MAX_GEN; // maxEvaluations
	private S bestSolution;
	

	public GeneticKMeansAlgorithm(int numGrupos, double pm,
			int n, int mAX_GEN, Instances base, int numInstances,
			DistanceFunction distance, Comparator<IntegerSolution> comparator, SelectionOperator<List<S>, S> selectionOp) {
		super((Problem<S>) new GKAProblem(base, numGrupos, distance));
		
		this.dataset = base;
		this.distance = distance;
		this.problem = new GKAProblem(base, numGrupos, this.distance);
		this.mutationOperator = new GKAMutation(pm, dataset, dataset.numInstances(), distance);
		this.kmeansOperator = new KMeansOperator(base, numInstances, distance,
				comparator);
		this.selectionOperator = selectionOp;
		N = n;
		MAX_GEN = mAX_GEN;
				
	}

	@Override
	protected void initProgress() {
		geno = MAX_GEN;
	}

	@Override
	protected void updateProgress() {
		geno--;
	}

	@Override
	protected boolean isStoppingConditionReached() {
		return geno == 0;
	}

	@Override
	protected List<S> createInitialPopulation() {
		List<S> population = new ArrayList<>(N);
		for (int i = 0; i < N; i++) {
			S newIndividual = (S) problem.createSolution();
			population.add(newIndividual);
		}
		return population;
	}

	@Override
	protected List<S> evaluatePopulation(List<S> population) {
		double[] fitnesses = new double[population.size()];

		int i = 0;
		for (S solution : population) {
			this.problem.evaluate((IntegerSolution) solution);
			fitnesses[i] = solution.getObjective(0);
		}

		double product = Mathematics.mean(fitnesses)
				* Mathematics.deviation(fitnesses);

		for (S solution : population) {
			solution.setObjective(0, (solution.getObjective(0) - product));
		}

		return population;
	}

	@Override
	protected List<S> selection(List<S> population) {
		List<S> selected = new ArrayList<S>(1);
		selected.add(selectionOperator.execute(population));

		return selected;
	}

	protected List<S> mutation(List<S> population) {
		S hatS;
		for (int solution = 0; solution < population.size(); solution++) {
			hatS = selection(population).get(0);
			population.add((S) mutationOperator.execute((IntegerSolution) hatS));
			population.remove(solution);
		}

		return population;
	}

	@Override
	protected List<S> reproduction(List<S> population) {
		for (int solution = 0; solution < population.size(); solution++) {
			population.add((S) kmeansOperator
					.execute((IntegerSolution) population.get(solution)));
			population.remove(solution);
		}

		return population;
	}

	@Override
	protected List<S> replacement(List<S> population,
			List<S> offspringPopulation) {
		return null;
	}
	

	protected double TWCV(S s){
		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();
		dataset = assigner.assign((DefaultIntegerSolution) s, dataset);
		return Mathematics.totalWithinClusterVariationOfCluster(dataset, distance);
	}

	protected S sWithMinimumSEMeasure(List<S> population) {
		S best = population.get(0);
				
		double bestTWCV = TWCV(best);
				
		double candidateTWCV;
		for (int i = 0; i < population.size(); i++) {
			candidateTWCV = TWCV(population.get(i));
			
			if(bestTWCV > candidateTWCV){
				best = population.get(i);
				bestTWCV = candidateTWCV;
			}
		}
		
		return best;
	}
		
	@Override
	public void run() {
		List<S> population;
		S s;

		setPopulation(createInitialPopulation());
		population = getPopulation();

		bestSolution = population.get(0);

		initProgress();
		while (!isStoppingConditionReached()) {
			setPopulation(evaluatePopulation(population));
			population = getPopulation();

			population = mutation(population);
			population = reproduction(population);

			s = sWithMinimumSEMeasure(population);
			
			if(TWCV(bestSolution) > TWCV(s)){
				bestSolution = s;
			}
			
			updateProgress();
		}
	}

	@Override
	public S getResult() {
		return bestSolution;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
