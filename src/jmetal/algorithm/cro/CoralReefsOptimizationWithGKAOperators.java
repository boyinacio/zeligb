package jmetal.algorithm.cro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jmetal.operators.mutation.KMeansOperator;

import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.util.pseudorandom.impl.MersenneTwisterGenerator;

import weka.core.Instances;
import zeligB.util.Mathematics;

public class CoralReefsOptimizationWithGKAOperators<S extends Solution<?>>
		extends AbstractCoralReefsOptimization<S, List<S>> {

	private Problem<S> problem;
	private int maxEvaluations;
	private int evaluations;
	private MersenneTwisterGenerator random;
	private KMeansOperator mutationOp; 
		
	public CoralReefsOptimizationWithGKAOperators(Problem<S> problem,
			int maxEvaluations, Comparator<S> comparator,
			SelectionOperator<List<S>, S> selectionOperator,
			CrossoverOperator<S> crossoverOperator,
			MutationOperator<S> mutationOperator, int n, int m, double rho,
			double fbs, double fa, double pd, int attemptsToSettle, KMeansOperator kmeans) {

		super(comparator, selectionOperator, crossoverOperator,
				mutationOperator, n, m, rho, fbs, fa, pd, attemptsToSettle);
		
		this.mutationOp = kmeans;
		this.problem = problem;
		this.maxEvaluations = maxEvaluations;
		this.random = new MersenneTwisterGenerator();

	}
		
	private static final long serialVersionUID = 3013223456538143239L;

	@Override
	protected void initProgress() {
		evaluations = 0;
	}

	@Override
	protected void updateProgress() {
		evaluations++;
	}

	@Override
	protected boolean isStoppingConditionReached() {
		return evaluations == maxEvaluations;
	}

	@Override
	protected List<S> createInitialPopulation() {
		List<S> population = new ArrayList<>(getN() * getM());

		int quantity = (int) (getRho() * getN() * getM());

		for (int i = 0; i < quantity; i++) {
			S newIndividual = problem.createSolution();
			population.add(newIndividual);
		}
		return population;
	}

	@Override
	protected List<Coordinate> generateCoordinates() {
		int popSize = getPopulationSize();
		MersenneTwisterGenerator random = new MersenneTwisterGenerator();

		ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>(popSize);

		for (int i = 0; i < popSize; i++) {
			coordinates.add(new Coordinate(random.nextInt(0, getN() - 1),
					random.nextInt(0, getM() - 1)));
		}

		return coordinates;
	}

	@Override
	protected List<S> evaluatePopulation(List<S> population) {
		double[] fitnesses = new double[population.size()];

		int i = 0;
		for (S solution : population) {
			this.problem.evaluate(solution);
			fitnesses[i] = solution.getObjective(0);
		}

		double product = Mathematics.mean(fitnesses)
				* Mathematics.deviation(fitnesses);

		for (S solution : population) {
			solution.setObjective(0, (solution.getObjective(0) - product));
		}

		return population;
	}

	@Override
	protected List<S> selectBroadcastSpawners(List<S> population) {
		int quantity = (int) (getFbs() * population.size());

		if ((quantity % 2) == 1) {
			quantity--;
		}

		List<S> spawners = new ArrayList<S>(quantity);

		for (int i = 0; i < quantity; i++) {
			S solution = selectionOperator.execute(population);
			spawners.add(solution);
		}

		return spawners;
	}

	@Override
	protected List<S> sexualReproduction(List<S> broadcastSpawners) {
		int sz = broadcastSpawners.size();

		List<S> larvae = new ArrayList<S>(sz);

		for (int i = 0; i < sz; i++) {
			larvae.add((S) mutationOp.execute((IntegerSolution) broadcastSpawners.get(i)));
		}

		return larvae;
	}

	@Override
	protected List<S> asexualReproduction(List<S> brooders) {
		int sz = brooders.size();

		List<S> larvae = new ArrayList<S>(sz);

		for (int i = 0; i < sz; i++) {
			larvae.add(mutationOperator.execute(brooders.get(i)));
		}

		return larvae;
	}

	@Override
	protected List<S> larvaeSetting(List<S> larvae, List<S> population,
			List<Coordinate> coordinates) {

		int attempts = getAttemptsToSettle();
		int index;

		for (S larva : larvae) {

			for (int attempt = 0; attempt < attempts; attempt++) {
				Coordinate C = new Coordinate(random.nextInt(0, getN() - 1),
						random.nextInt(0, getM() - 1));

				if (!coordinates.contains(C)) {
					population.add(larva);
					coordinates.add(C);
					break;
				}

				index = coordinates.indexOf(C);

				if (comparator.compare(larva, population.get(index)) < 0) {
					population.add(index, larva);
					population.remove(index + 1);
					break;
				}

			}

		}

		return population;
	}

	@Override
	protected List<S> depredation(List<S> population,
			List<Coordinate> coordinates) {
		int popSize = population.size();
		int quantity = (int) (getFd() * popSize);

		quantity = popSize - quantity;

		double coin;
		for (int i = popSize-1; i > quantity; i--) {
			coin = random.nextDouble();
			
			if(coin < getPd()){
				population.remove(population.size()-1);
				coordinates.remove(population.size()-1);	
			}
			
		}
				
		return population;
	}

	@Override
	public List<S> getResult() {
		Collections.sort(getPopulation(), comparator) ;
		return getPopulation();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
