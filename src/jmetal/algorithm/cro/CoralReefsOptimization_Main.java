package jmetal.algorithm.cro;

import java.util.Comparator;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.problem.singleobjective.Sphere;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.comparator.ObjectiveComparator;

public class CoralReefsOptimization_Main {

	public static void main(String[] args) {
		Problem<DoubleSolution> problem;
		Algorithm<List<DoubleSolution>> algorithm;
		CrossoverOperator<DoubleSolution> crossover;
		MutationOperator<DoubleSolution> mutation;
		SelectionOperator<List<DoubleSolution>, DoubleSolution> selection;

//		int CH = 90;
//		problem = new DisciplinesNumberProblem(CH);
		
		int dimensions = 1;
		problem = new Sphere(dimensions);

		double crossoverProbability = 1 / problem.getNumberOfVariables();
		double distributionIndex = 20.0;
		crossover = new SBXCrossover(crossoverProbability, distributionIndex);
//		crossover = new HeritageCrossover();

		double mutationProbability = 0.5;
		mutation = new PolynomialMutation(mutationProbability, distributionIndex);
//		mutation = new MaskMutation(mutationProbability);

		Comparator<DoubleSolution> comparator = new ObjectiveComparator<DoubleSolution>(0);
		selection = new BinaryTournamentSelection<DoubleSolution>(comparator);
//		selection = new SingleObjectiveMinimumRouletteWheelSelection<IntegerSolution>();

		int maxGenerations = 100;
		int n = 5;
		int m = 10;
		double rho = 0.7;
		double fbs = 0.9;
		double fa = 0.1;
		double pd = 0.5;
		int attemptsToSettle = 3;
		algorithm = new CoralReefsOptimization<DoubleSolution>(
				problem, maxGenerations, comparator, selection,
				crossover, mutation, n, m, rho, fbs, fa, pd,
				attemptsToSettle);

		long st = System.currentTimeMillis();
		
		
		algorithm.run();
		long end = System.currentTimeMillis();

		List<DoubleSolution> population = algorithm.getResult();
		long computingTime = end-st;

		JMetalLogger.logger.info("Total execution time: " + computingTime
				+ "ms");

		printSolutions(population);

		// AbstractAlgorithmRunner.printFinalSolutionSet(population);

	}

	public static void printSolutions(List<DoubleSolution> population) {
		for (Solution S : population) {
			int nVars = S.getNumberOfVariables();

			for (int i = 0; i < nVars; i++) {
				System.out.print(S.getVariableValueString(i) + " ");
			}

			System.out.print(" (fitness: " + S.getObjective(0) + ")\n");

		}
	}

}
