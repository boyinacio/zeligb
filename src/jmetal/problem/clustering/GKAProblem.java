package jmetal.problem.clustering;

import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal.problem.impl.AbstractIntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import weka.core.DistanceFunction;
import weka.core.Instances;
import zeligB.util.Mathematics;
import zeligB.util.assigner.ClusterNumberAssigner;

public class GKAProblem extends AbstractIntegerProblem {

	private Instances baseDados;
	private DistanceFunction distance;
	private int constant, maxGrupos;
		
	public GKAProblem(Instances baseDados, int maxGrupos, DistanceFunction distance) {
		super();
		this.baseDados = baseDados;
		this.distance = distance;
		this.constant = 2;
		this.maxGrupos = maxGrupos;
		
		setName("GKA_Problem");
		setNumberOfVariables(this.baseDados.numInstances());
		setNumberOfObjectives(1);
		
		List<Integer> lowerLimit = new ArrayList<Integer>(getNumberOfVariables());
		List<Integer> upperLimit = new ArrayList<Integer>(getNumberOfVariables());
		
		for (int i = 0; i < getNumberOfVariables(); i++) {
			lowerLimit.add(1);
			upperLimit.add(this.maxGrupos);
		}
		
		setLowerLimit(lowerLimit);
		setUpperLimit(upperLimit);
						
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();
		baseDados = assigner.assign((DefaultIntegerSolution) solution, baseDados);
		double TWCV = Mathematics.totalWithinClusterVariationOfCluster(this.baseDados, this.distance);
		
		solution.setObjective(0, TWCV);
	}

}
