package jmetal.problem.clustering;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal.problem.impl.AbstractIntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import weka.core.Instances;
import zeligB.util.assigner.ClusterNumberAssigner;
import zeligB.util.clusteringEvaluators.BasicDaviesBouldin;

public class SimpleClusteringProblem extends AbstractIntegerProblem {
	
	private static final long serialVersionUID = 4261462154623330634L;

	private Instances baseDados;
	private int maxGrupos;
	private ClusterNumberAssigner<DefaultIntegerSolution> assigner;
		
	public SimpleClusteringProblem(Instances baseDados, int maxGrupos) {
		super();
		this.baseDados = baseDados;
		this.maxGrupos = maxGrupos;
		this.assigner = new ClusterNumberAssigner<DefaultIntegerSolution>();
		
		setName("SimpleClusteringProblem");
		setNumberOfVariables(this.baseDados.numInstances());
		setNumberOfObjectives(1);
		
		List<Integer> lowerLimit = new ArrayList<Integer>(getNumberOfVariables());
		List<Integer> upperLimit = new ArrayList<Integer>(getNumberOfVariables());
		
		for (int i = 0; i < getNumberOfVariables(); i++) {
			lowerLimit.add(1);
			upperLimit.add(this.maxGrupos);
		}
		
		setLowerLimit(lowerLimit);
		setUpperLimit(upperLimit);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		Instances newDataset = assigner.assign((DefaultIntegerSolution) solution, baseDados);
		try {
			solution.setObjective(0, BasicDaviesBouldin.main(newDataset));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
