package jmetal.operators.mutation;

import java.util.Comparator;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;
import zeligB.util.Mathematics;
import zeligB.util.assigner.ClusterNumberAssigner;

public class KMeansOperator implements MutationOperator<IntegerSolution> {

	private Instances base;
	private int numInstances;
	private DistanceFunction distance;
	private Comparator<IntegerSolution> comparator;
	private ClusterNumberAssigner<IntegerSolution> assigner;

	public KMeansOperator(Instances base, int numInstances,
			DistanceFunction distance, Comparator<IntegerSolution> comparator) {
		super();
		this.base = base;
		this.numInstances = numInstances;
		this.distance = distance;
		this.comparator = comparator;
		this.assigner = new ClusterNumberAssigner<IntegerSolution>();
	}

	@Override
	public IntegerSolution execute(IntegerSolution source) {
		base = assigner.assign((DefaultIntegerSolution) source, base);

		Instance[] centroids = Mathematics.centroidsOf(base);

		double[] distanceToCentroids = new double[centroids.length];
		double leastDistance;
		
		int numInstances = base.numInstances();

		for (int i = 0; i < numInstances; i++) {

			for (int j = 0; j < distanceToCentroids.length; j++) {
				distanceToCentroids[j] = distance.distance(
						base.instance(i), centroids[j]);
			}

			leastDistance = distanceToCentroids[0];

			for (int j = 0; j < distanceToCentroids.length; j++) {
				if (leastDistance > distanceToCentroids[j]) {
					leastDistance = distanceToCentroids[j];
					source.setVariableValue(i, j);
				}
			}

		}

		return source;
	}

}
