package jmetal.operators.mutation;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.pseudorandom.impl.MersenneTwisterGenerator;

import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;
import zeligB.util.Mathematics;
import zeligB.util.assigner.ClusterNumberAssigner;

public class GKAMutation implements MutationOperator<IntegerSolution> {

	private double mutationProbability_;
	private Instances base;
	private int numInstances;
	private DistanceFunction distanceMeasure;
	private MersenneTwisterGenerator random;

	public GKAMutation(double mutationProbability_, Instances base,
			int numInstances, DistanceFunction distanceMeasure) {
		super();
		this.mutationProbability_ = mutationProbability_;
		this.base = base;
		this.numInstances = numInstances;
		this.distanceMeasure = distanceMeasure;
		this.random = new MersenneTwisterGenerator();
	}

	@Override
	public IntegerSolution execute(IntegerSolution source) {
		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();

		base = assigner.assign((DefaultIntegerSolution) source, base);

		Instance[] centroids;
		double[] distancesToCentroids;
		double[] clustersProbabilities;
		double dmax, sumOfDistances, sumOfProbabilities = 0, selector;
		double cm = random.nextInt(2, 4);

		for (int i = 0; i < numInstances; i++) {
			if (random.nextDouble() < mutationProbability_) {
				centroids = Mathematics.centroidsOf(this.base);
				distancesToCentroids = new double[centroids.length];
				clustersProbabilities = new double[centroids.length];

				for (int j = 0; j < distancesToCentroids.length; j++) {
					distancesToCentroids[j] = distanceMeasure.distance(
							base.instance(i), centroids[j]);
				}

				if (distanceMeasure.distance(base.instance(i),
						centroids[(int) base.instance(i).classValue()]) > 0) {

					dmax = distancesToCentroids[0];
					for (int j = 0; j < distancesToCentroids.length; j++) {
						if (dmax > distancesToCentroids[j]) {
							dmax = distancesToCentroids[j];
						}
					}

					sumOfDistances = 0;
					for (int j = 0; j < distancesToCentroids.length; j++) {
						sumOfDistances += ((cm * dmax) - distancesToCentroids[j]);
					}

					for (int j = 0; j < centroids.length; j++) {
						clustersProbabilities[j] = ((cm * dmax) - distancesToCentroids[j])
								/ sumOfDistances;
					}

					for (int j = 0; j < clustersProbabilities.length; j++) {
						sumOfProbabilities += clustersProbabilities[j];
					}

					selector = random.nextDouble(0, sumOfProbabilities);
					sumOfProbabilities = 0;

					for (int j = 0; j < clustersProbabilities.length; j++) {
						sumOfProbabilities += clustersProbabilities[j];
						if (sumOfProbabilities >= selector) {
							source.setVariableValue(i, j);
							break;
						}
					}
				}
			}
		}

		return source;
	}

}
