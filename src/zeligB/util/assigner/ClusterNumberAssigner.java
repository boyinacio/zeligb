package zeligB.util.assigner;

import java.util.Arrays;
import java.util.HashMap;

import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;

public class ClusterNumberAssigner<IntegerSolution> implements Assigner<DefaultIntegerSolution> {

	@Override
	public Instances assign(DefaultIntegerSolution solution, Instances dataset) {
		int[] labels = new int[solution.getNumberOfVariables()];
		
		for (int i = 0; i < labels.length; i++) {
			labels[i] = solution.getVariableValue(i);
		}
		
		return translate(labels,dataset);
	}

	public Instances translate(int[] labels, Instances instances) {
		if(instances.classIndex() > -1){
			// Coloca a classe para ser o primeiro atributo (para poder-se mexer na
			// verdadeira classe)
			instances.setClassIndex(0);

			// Coloca para a classe ser um atributo cluster
			instances.deleteAttributeAt(instances.numAttributes() - 1);	
		}
		
		// Copia o atributo "labels" para a variável aux
		int[] aux = new int[labels.length];
		for (int i = 0; i < aux.length; i++) {
			aux[i] = labels[i];
		}

		// Ordena aux (ela vai servir para gerar os labels das classes a serem
		// colocados na base
		Arrays.sort(aux);

		// Clusters aqui é usado para ajudar a fazer a *lista de labels dos
		// clusters*
		String[] clusters = getLabels(aux);

		FastVector attributeValues = new FastVector();
		for (int i = 0; i < clusters.length; i++) {
			if ((i > 0) && (clusters[i].compareTo(clusters[i - 1]) == 0)) {
				continue;
			}
			attributeValues.addElement(clusters[i]);
		}

		// Vetor de inteiros numeros de 0 ao tamanho de "attributeValues"
		int[] realClusters = new int[attributeValues.size()];
		for (int i = 0; i < realClusters.length; i++) {
			realClusters[i] = i + 1;
		}

		String[] grupos = getLabels(realClusters);

		/*
		 * Este mapa faz a relação entre os labels de "attributeValues" e
		 * "realClusters" de modo que os clusters fiquem com labels de 0 a
		 * clusters-1
		 */
		HashMap<String, String> mapa = new HashMap<String, String>();
		for (int i = 0; i < grupos.length; i++) {
			mapa.put((String) attributeValues.elementAt(i), grupos[i]);
		}

		attributeValues.removeAllElements();

		for (int i = 0; i < grupos.length; i++) {
			attributeValues.addElement(grupos[i]);
		}

		// Aqui, "clusters" é usado para colocar em cada instância seu
		// respectivo class label
		clusters = getLabels(labels);

		instances.insertAttributeAt(new Attribute("class", attributeValues),
				instances.numAttributes());

		// Recoloca a classe como o último atributo
		instances.setClassIndex(instances.numAttributes() - 1);

		for (int i = 0; i < instances.numInstances(); i++) {
			instances.instance(i).setValue(instances.numAttributes() - 1,
					mapa.get(clusters[i]));
		}

		return instances;
	}

	private String[] getLabels(int[] v) {
		String[] labelsS = new String[v.length];

		for (int i = 0; i < labelsS.length; i++) {
			labelsS[i] = "cluster" + v[i];
		}

		return labelsS;
	}
	
}
