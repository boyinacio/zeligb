package zeligB.util.assigner;

import org.uma.jmetal.solution.Solution;

import weka.core.Instances;

public interface Assigner<S extends Solution<?>> {
	
	public Instances assign(S solution, Instances dataset);

}
