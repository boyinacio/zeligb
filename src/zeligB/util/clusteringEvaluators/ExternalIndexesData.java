package zeligB.util.clusteringEvaluators;

import weka.core.Instances;

public class ExternalIndexesData {
	
	public static double[] dataForExternalIndexes(Instances original, Instances modified){
		double a1, a2, a3, a4;

		a1 = a2 = a3 = a4 = 0;

		int numInstances = original.numInstances();

		double xUOnOriginal, xUOnModified, xVOnOriginal, xVOnModified;
		for (int i = 0; i < numInstances; i++) {
			for (int j = 0; j < numInstances; j++) {
				xUOnOriginal = original.instance(i).classValue();
				xUOnModified = modified.instance(i).classValue();

				xVOnOriginal = original.instance(j).classValue();
				xVOnModified = modified.instance(j).classValue();

				if ((xUOnModified == xVOnModified)
						&& (xUOnOriginal == xVOnOriginal)) {
					a1++;
				} else if ((xUOnModified == xVOnModified)
						&& (xUOnOriginal != xVOnOriginal)) {
					a2++;
				} else if ((xUOnModified != xVOnModified)
						&& (xUOnOriginal == xVOnOriginal)) {
					a3++;
				} else {
					a4++;
				}
			}
		}
		
		double[] result = {a1,a2,a3,a4,(a1+a2+a3+a4),(a1+a2),(a1+a3)}; 
		
		return result;
	}

}
