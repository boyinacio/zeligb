package zeligB.util.distances;

import weka.core.Instance;
import weka.core.NormalizableDistance;

public class JoaoCarlosDistance extends NormalizableDistance {
	
	private static final long serialVersionUID = 1433024865923731536L;

	@Override
	public String getRevision() {
		return "Versão 1.0, Revisão 0";
	}

	@Override
	public String globalInfo() {
		return "Executa um método de distância desenvolvido pelo Prof. João Carlos.";
	}

	@Override
	public double distance(Instance A, Instance B) {
		double diff = 0;
		for (int i = 1; i < A.numAttributes() - 1; i++) {
			if ( A.attribute( i ).isNominal() ) {
			   diff += A.value( i ) == B.value( i ) ? 0 : 1;
			}					
			// atributo numerico
			else{
			   diff += Math.abs( A.value( i ) - B.value( i ) );
			}				
		}
		return diff;
	  }
	
	@Override
	protected double updateDistance(double currDist, double diff) {
		return diff;
	}

		
}
