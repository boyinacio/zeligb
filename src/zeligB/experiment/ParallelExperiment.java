package zeligB.experiment;

import java.util.List;

public class ParallelExperiment {
	
	private List<? extends Experiment> experiments;
	private Thread[] threads;

	public ParallelExperiment(List<? extends Experiment> experiments) {
		this.experiments = experiments;
		threads = new Thread[this.experiments.size()];
		
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(experiments.get(i));
		}
	} 
	
	public void start(){
		for (int i = 0; i < threads.length; i++) {
			threads[i].start();
		}
	}
	
	public boolean experimentsRunning(){
		for (int i = 0; i < threads.length; i++) {
			if(threads[i].isAlive()){
				return true;
			}
		}
		return false;
	}

}
