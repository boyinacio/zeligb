package zeligB.experiment;

import java.io.Serializable;

public interface Experiment<R> extends Runnable, Serializable {
	
	public void run();
	public R getResult();	
	
}
