package zeligB.experiment.runner;

import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal.util.pseudorandom.impl.MersenneTwisterGenerator;

import weka.clusterers.Cobweb;
import weka.clusterers.DBSCAN;
import weka.clusterers.EM;
import weka.core.DistanceFunction;
import zeligB.experiment.ParallelExperiment;
import zeligB.experiment.clustering.classic.KEstimationExperiment;
import zeligB.util.distances.JoaoCarlosDistance;

public class KEstimationExperimentRunner {

	public static void main(String[] args) {
		String basename = "ronda";
		String dataset = "datasets/" + basename + ".arff";
		DistanceFunction distance = new JoaoCarlosDistance();

		double eps = 0.05, max_eps = 1;
		int minPoints = 3, maxPoints = 10;

		DBSCAN dbscan;
		KEstimationExperiment estDBSCAN;
		String filename;

		System.out.println("Algoritmo\tEps\tMinPts\t#Grupos\tDBIndex\tSillhouette");

		for (double i = eps; i <= max_eps; i += eps) {
			for (int points = minPoints; points <= maxPoints; points++) {
				dbscan = new DBSCAN();
				dbscan.setEpsilon(i);
				dbscan.setMinPoints(points);

				estDBSCAN = new KEstimationExperiment(dbscan, dataset, 0,
						distance, true);

				estDBSCAN.run();

				List<Double> results = estDBSCAN.getResult();

				System.out.printf(
						"DBSCAN\t%.2f\t%d\t%d\t%.2f\t%.2f\n",
						i, points, estDBSCAN.numberOfGroups(),
						results.get(0), results.get(1));
				 
				filename = basename + "_dbscan_eps_" + i + "_minPts_"
						+ points + "_" + estDBSCAN.numberOfGroups() + "k.arff";
				estDBSCAN.writeClusteredInstancesToFile("results/" + filename);

			}

		}

		ArrayList<KEstimationExperiment> experiments = new ArrayList<KEstimationExperiment>();

		/** COBWEB **/
		/*********************************************************************/
		KEstimationExperiment estCobweb;
		Cobweb cobweb;

		cobweb = new Cobweb();
		cobweb.setSeed(new MersenneTwisterGenerator().nextInt(1000, 10000));

		estCobweb = new KEstimationExperiment(cobweb, dataset, 0, distance,
				true);
		experiments.add(estCobweb);
		/*********************************************************************/

		/** EM **/
		/*********************************************************************/
		KEstimationExperiment estEM;
		EM em;

		em = new EM();
		em.setSeed(new MersenneTwisterGenerator().nextInt(1000, 10000));

		estEM = new KEstimationExperiment(em, dataset, 0, distance, true);
		experiments.add(estEM);
		/*********************************************************************/

		ParallelExperiment executor = new ParallelExperiment(experiments);
		executor.start();
		while (executor.experimentsRunning())
			;

		filename = basename + "_em_" + estEM.numberOfGroups() + "k.arff";
		estEM.writeClusteredInstancesToFile("results/" + filename);

		filename = basename + "_cobweb_" + estCobweb.numberOfGroups()
				+ "k.arff";
		estCobweb.writeClusteredInstancesToFile("results/" + filename);

		System.out.printf("Cobweb\t0\t0\t%d\t%.2f\t%.2f\n",
				estCobweb.numberOfGroups(), estCobweb.getResult().get(0),
				estCobweb.getResult().get(1));

		System.out.printf("EM\t0\t0\t%d\t%.2f\t%.2f\n", estEM.numberOfGroups(), estEM
				.getResult().get(0), estEM.getResult().get(1));

//		 System.out.println("EM formou " + estEM.numberOfGroups() +
//		 " grupos");
//		 System.out.println("Cobweb formou " + estCobweb.numberOfGroups()
//		 + " grupos");

	}

}
