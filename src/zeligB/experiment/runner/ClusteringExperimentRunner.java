package zeligB.experiment.runner;

import java.util.List;

import weka.clusterers.DBSCAN;
import weka.core.DistanceFunction;
import zeligB.experiment.clustering.ClusteringExperiment;
import zeligB.experiment.clustering.classic.SimpleClusteringExperiment;
import zeligB.experiment.clustering.evolutionary.CROClusteringExperiment;
import zeligB.experiment.clustering.evolutionary.CROWithGKAOperatorsClusteringExperiment;
import zeligB.experiment.clustering.evolutionary.GKAClusteringExperiment;
import zeligB.util.distances.JoaoCarlosDistance;

public class ClusteringExperimentRunner {

	public static void main(String[] args) {
		String dataset = "datasets/iris.arff";
		DistanceFunction distance = new JoaoCarlosDistance();

		DBSCAN dbscan = new DBSCAN();
		dbscan.setEpsilon(0.8);
		dbscan.setMinPoints(3);

		ClusteringExperiment CRO, GKA, KMEANS, DBScan, CROGKA;
				
		CRO = new CROClusteringExperiment(dataset, distance);
		GKA = new GKAClusteringExperiment(dataset, distance);
		CROGKA = new CROWithGKAOperatorsClusteringExperiment(dataset, distance);
		KMEANS = new SimpleClusteringExperiment(dataset, distance,true);
		DBScan = new SimpleClusteringExperiment(dbscan, dataset, 3,
				new JoaoCarlosDistance(),true);
								
		
		Thread[] experiments = new Thread[5];
		experiments[0] = new Thread(CRO);
		experiments[1] = new Thread(GKA);
		experiments[2] = new Thread(CROGKA);
		experiments[3] = new Thread(KMEANS);
		experiments[4] = new Thread(DBScan);
		
		for (int thread = 0; thread < experiments.length; thread++) {
			experiments[thread].start();
		}

		boolean flag = true;
		while(flag){
			int count = 0;
			for (int thread = 0; thread < experiments.length; thread++) {
				if(!experiments[thread].isAlive()){
					count++;
				}
			}
			
			if(count == 5){
				flag = false;
			}
			
		}
		
		List<Double> croResults = CRO.getResult();
		List<Double> gkaResults = GKA.getResult();
		List<Double> crogkaResults = CROGKA.getResult();
		List<Double> kmeansResults = KMEANS.getResult();
		List<Double> dbscanResults = DBScan.getResult();
		
		System.out.println("ALG\tDB\tRAND\tMX\tJaccard");

		/**********************************************/
		System.out.print("CRO: ");

		for (Double index : croResults) {
			System.out.print(index + " ");
		}

		System.out.println();
		/**********************************************/

		/**********************************************/
		System.out.print("GKA: ");

		for (Double index : gkaResults) {
			System.out.print(index + " ");
		}

		System.out.println();
		/**********************************************/
		
		/**********************************************/
		System.out.print("CRO_GKA: ");

		for (Double index : crogkaResults) {
			System.out.print(index + " ");
		}

		System.out.println();
		/**********************************************/

		/**********************************************/
		System.out.print("KMEANS: ");

		for (Double index : kmeansResults) {
			System.out.print(index + " ");
		}

		System.out.println();
		/**********************************************/

		/**********************************************/
		System.out.print("DBScan: ");

		for (Double index : dbscanResults) {
			System.out.print(index + " ");
		}
		
		System.out.println();
		/**********************************************/
	}

}
