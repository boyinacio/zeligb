package zeligB.experiment.clustering.evolutionary;

import java.io.IOException;
import java.util.Comparator;

import jmetal.algorithm.ga.GeneticKMeansAlgorithm;
import jmetal.operators.selection.SingleObjectiveMinimumRouletteWheelSelection;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.comparator.ObjectiveComparator;

import weka.core.DistanceFunction;
import weka.core.Instances;
import zeligB.experiment.clustering.ClusteringExperiment;
import zeligB.util.assigner.ClusterNumberAssigner;
import zeligB.util.clusteringEvaluators.AdjustedRandIndex;
import zeligB.util.clusteringEvaluators.BasicDaviesBouldin;
import zeligB.util.clusteringEvaluators.MXIndex;

public class GKAClusteringExperiment extends EvolutionaryClusteringExperiment {
	
	private static final long serialVersionUID = -4367311567418005490L;
	
	private Algorithm<DefaultIntegerSolution> algorithm;
	
	public GKAClusteringExperiment(String dataset,
			DistanceFunction distanceFunction) {
		super(dataset, distanceFunction);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void run() {
		loadAlgorithm();
		
		algorithm.run();

		IntegerSolution solution = algorithm.getResult();
		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();

		Instances newDataset = assigner.assign(
				(DefaultIntegerSolution) solution, new Instances(dataset));

		try {
			results.add(BasicDaviesBouldin.main(newDataset));
			results.add(AdjustedRandIndex.ARI(dataset, newDataset));
			results.add(MXIndex.evaluate(newDataset, distanceFunction, 0.8));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	@Override
	public void loadAlgorithm() {
		int numInstances = dataset.numInstances();

		int n = 50;
		int mAX_GEN = 100;

		double pm = 0.05;
		int numGrupos = dataset.numClasses();

		Comparator<IntegerSolution> comparator = new ObjectiveComparator<IntegerSolution>(
				0);

		SingleObjectiveMinimumRouletteWheelSelection<DefaultIntegerSolution> selection = new SingleObjectiveMinimumRouletteWheelSelection<DefaultIntegerSolution>();

		algorithm = new GeneticKMeansAlgorithm<DefaultIntegerSolution>(
				numGrupos, pm, n, mAX_GEN, dataset, numInstances, distanceFunction,
				comparator, selection);
		
	}

}
