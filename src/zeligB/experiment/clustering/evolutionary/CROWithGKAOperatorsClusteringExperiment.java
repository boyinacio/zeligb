package zeligB.experiment.clustering.evolutionary;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jmetal.algorithm.cro.CoralReefsOptimizationWithGKAOperators;
import jmetal.operators.crossover.HeritageCrossover;
import jmetal.operators.mutation.GKAMutation;
import jmetal.operators.mutation.KMeansOperator;
import jmetal.operators.selection.SingleObjectiveMinimumRouletteWheelSelection;
import jmetal.problem.clustering.SimpleClusteringProblem;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.comparator.ObjectiveComparator;

import weka.core.DistanceFunction;
import weka.core.Instances;
import zeligB.experiment.clustering.ClusteringExperiment;
import zeligB.util.assigner.ClusterNumberAssigner;
import zeligB.util.clusteringEvaluators.AdjustedRandIndex;
import zeligB.util.clusteringEvaluators.BasicDaviesBouldin;
import zeligB.util.clusteringEvaluators.MXIndex;

public class CROWithGKAOperatorsClusteringExperiment extends
		EvolutionaryClusteringExperiment {
	
	private CrossoverOperator<IntegerSolution> crossover;
	private MutationOperator<IntegerSolution> mutation;
	private MutationOperator<IntegerSolution> kmeans;
	private SelectionOperator<List<IntegerSolution>, IntegerSolution> selection;

	private Comparator<IntegerSolution> comparator;

	public CROWithGKAOperatorsClusteringExperiment(String dataset,
			DistanceFunction distanceFunction) {
		super(dataset, distanceFunction);
	}

	private static final long serialVersionUID = -4836109882688646086L;

	@Override
	public void run() {
		loadAlgorithm();
		
		algorithm.run();

		List<IntegerSolution> solutions = (List<IntegerSolution>) algorithm.getResult();

		ClusterNumberAssigner<IntegerSolution> assigner = new ClusterNumberAssigner<IntegerSolution>();

		Instances newDataset;

		double db, rand, mx;

		Collections.sort(solutions, comparator);
		IntegerSolution best = solutions.get(0);

		newDataset = assigner.assign((DefaultIntegerSolution) best,
				new Instances(dataset));

		try {
			db = BasicDaviesBouldin.main(newDataset);
			rand = AdjustedRandIndex.ARI(dataset, newDataset);
			mx = MXIndex.evaluate(newDataset, distanceFunction, 0.8);

			results.add(db);
			results.add(rand);
			results.add(mx);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	@Override
	public void loadAlgorithm() {
		comparator = new ObjectiveComparator<IntegerSolution>(0);

		// double crossoverProbability = 1 / problem.getNumberOfVariables();
		// double distributionIndex = 20.0;
		// crossover = new SBXCrossover(crossoverProbability,
		// distributionIndex);
		crossover = new HeritageCrossover();
		// crossover = new KMeansOperator(super.dataset,
		// super.dataset.numInstances(), distanceFunction, comparator);

		double mutationProbability = 0.05;
		// mutation = new PolynomialMutation(mutationProbability,
		// distributionIndex);
		// mutation = new MaskMutation(mutationProbability);
		mutation = new GKAMutation(mutationProbability, dataset,
				dataset.numInstances(), distanceFunction);
		kmeans = new KMeansOperator(dataset, dataset.numInstances(),
				distanceFunction, comparator);

		problem = new SimpleClusteringProblem(this.dataset,
				this.dataset.numClasses());
		// selection = new
		// BinaryTournamentSelection<IntegerSolution>(comparator);
		selection = new SingleObjectiveMinimumRouletteWheelSelection<IntegerSolution>();

		int maxGenerations = 100;
		int n = 5;
		int m = 10;
		double rho = 0.7;
		double fbs = 0.9;
		double fa = 0.1;
		double pd = 0.5;
		int attemptsToSettle = 3;

		algorithm = new CoralReefsOptimizationWithGKAOperators<IntegerSolution>(
				problem, maxGenerations, comparator, selection, crossover,
				mutation, n, m, rho, fbs, fa, pd, attemptsToSettle,
				(KMeansOperator) kmeans);

	}

}
