package zeligB.experiment.clustering.evolutionary;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;

import weka.core.DistanceFunction;
import zeligB.experiment.clustering.ClusteringExperiment;

public abstract class EvolutionaryClusteringExperiment extends ClusteringExperiment {

	protected Algorithm algorithm;
	protected Problem problem;
		
	public EvolutionaryClusteringExperiment(String dataset,
			DistanceFunction distanceFunction) {
		super(dataset, distanceFunction);
	}

	private static final long serialVersionUID = -7840690597578961478L;

}
