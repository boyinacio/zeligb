package zeligB.experiment.clustering;

import java.util.ArrayList;
import java.util.List;

import weka.core.DistanceFunction;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zeligB.experiment.Experiment;
import zeligB.util.distances.JoaoCarlosDistance;

public abstract class ClusteringExperiment implements Experiment<List<Double>> {

	private static final long serialVersionUID = 1045544810046321221L;
	
	protected Instances dataset;
	protected DistanceFunction distanceFunction;
	protected List<Double> results;

	public ClusteringExperiment(String dataset,
			DistanceFunction distanceFunction) {
		super();
				
		loadDataset(dataset);
		
		if(distanceFunction == null){
			this.distanceFunction = new JoaoCarlosDistance();
		} else {
			this.distanceFunction = distanceFunction;	
		}
		
		this.results = new ArrayList<Double>();
	}
			
	private void loadDataset(String filename){
		try {
			DataSource source = new DataSource(filename);
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
		
		dataset = new Instances(dataset);
	}
	

	@Override
	public List<Double> getResult() {
		return results;
	}
	
	protected abstract void loadAlgorithm();
		
}