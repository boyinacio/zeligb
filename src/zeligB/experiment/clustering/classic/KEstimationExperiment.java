package zeligB.experiment.clustering.classic;

import weka.clusterers.Clusterer;
import weka.core.DistanceFunction;

public class KEstimationExperiment extends SimpleClusteringExperiment {

	private static final long serialVersionUID = 4956641787317938560L;

	public KEstimationExperiment(String dataset,
			DistanceFunction distanceFunction) {
		super(dataset, distanceFunction, false);
	}

	public KEstimationExperiment(Clusterer clusterer, String dataset,
			int numGrupos, DistanceFunction distanceFunction) {
		super(clusterer, dataset, numGrupos, distanceFunction, false);
		super.dataset.setClassIndex(-1);
	}
	
	public KEstimationExperiment(Clusterer clusterer, String dataset,
			int numGrupos, DistanceFunction distanceFunction, boolean calculateIndexes) {
		super(clusterer, dataset, numGrupos, distanceFunction, calculateIndexes);
		super.dataset.setClassIndex(-1);
	}

	public int numberOfGroups() {
		return getClusteredInstances().numClasses();
	}

}
