package zeligB.experiment.clustering.classic;

import java.io.File;
import java.io.IOException;

import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.util.pseudorandom.impl.MersenneTwisterGenerator;

import weka.clusterers.Clusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.DistanceFunction;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zeligB.experiment.clustering.ClusteringExperiment;
import zeligB.util.assigner.ClusterNumberAssigner;
import zeligB.util.clusteringEvaluators.AdjustedRandIndex;
import zeligB.util.clusteringEvaluators.BasicDaviesBouldin;
import zeligB.util.clusteringEvaluators.JaccardCoefficient;
import zeligB.util.clusteringEvaluators.MXIndex;
import zeligB.util.clusteringEvaluators.Sillhouette;

public class SimpleClusteringExperiment<R> extends ClusteringExperiment {

	private Clusterer clusterer;
	private Instances copy;
	private int numGrupos;
	private MersenneTwisterGenerator random;
	private ClusterNumberAssigner<IntegerSolution> assigner;
	private boolean calculateIndexes;

	public SimpleClusteringExperiment(String dataset,
			DistanceFunction distanceFunction, boolean calculateIndexes) {
		super(dataset, distanceFunction);
		this.calculateIndexes = calculateIndexes;
		clusterer = new SimpleKMeans();
		numGrupos = super.dataset.numClasses();
		random = new MersenneTwisterGenerator();
		assigner = new ClusterNumberAssigner<IntegerSolution>();
		prepareKMeans();

	}

	/**
	 * Construtor com parâmetos preespecificados. Esse clusterer já tem que vir
	 * configurado
	 * 
	 * @param clusterer
	 * @param dataset
	 * @param numGrupos
	 * @param distanceFunction
	 */
	public SimpleClusteringExperiment(Clusterer clusterer, String dataset,
			int numGrupos, DistanceFunction distanceFunction,
			boolean calculateIndexes) {
		super(dataset, distanceFunction);
		this.clusterer = clusterer;
		this.numGrupos = numGrupos;
		this.calculateIndexes = calculateIndexes;
		random = new MersenneTwisterGenerator();
		assigner = new ClusterNumberAssigner<IntegerSolution>();
	}

	public void setNumGrupos(int grupos) {
		if (grupos > 1) {
			this.numGrupos = grupos;
		}
	}

	private static final long serialVersionUID = 1674212026740083770L;

	@Override
	public void run() {
		deleteClassAttribute();

		try {
			clusterer.buildClusterer(copy);

			int[] labels = new int[copy.numInstances()];

			for (int i = 0; i < labels.length; i++) {
				try {
					labels[i] = clusterer.clusterInstance(copy.instance(i));
				} catch (Exception e) {
					labels[i] = -1;
				}
			}

			copy = assigner.translate(labels, copy);

			if (this.calculateIndexes) {
				double db = BasicDaviesBouldin.main(copy);
				double rand = AdjustedRandIndex.ARI(dataset, copy);
				double mx = MXIndex.evaluate(copy, distanceFunction, 0.8);
				double jaccard = JaccardCoefficient.calculate(dataset, copy);
				double sillhouette = Sillhouette.Silhouette(copy);

				results.add(db);
				results.add(sillhouette);
				results.add(rand);
				results.add(mx);
				results.add(jaccard);

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void loadAlgorithm() {
	}

	public void deleteClassAttribute() {
		if (dataset.classIndex() > -1) {
			copy = new Instances(dataset);
			Remove filter = new Remove();

			try {
				filter.setAttributeIndices("" + (dataset.classIndex() + 1));
				filter.setInputFormat(dataset);
				copy = Filter.useFilter(dataset, filter);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
		} else {
			copy = dataset;
		}

	}

	private void prepareKMeans() {

		try {
			((SimpleKMeans) clusterer).setPreserveInstancesOrder(true);
			((SimpleKMeans) clusterer).setNumClusters(this.numGrupos);
			((SimpleKMeans) clusterer).setSeed(random.nextInt(1000, 10000));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public Instances getClusteredInstances() {
		return copy;
	}

	public void writeClusteredInstancesToFile(String filename) {
		ArffSaver saver = new ArffSaver();
		saver.setInstances(copy);
		try {
			saver.setFile(new File(filename));
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public Clusterer getClusterer() {
		return clusterer;
	}

	public void setClusterer(Clusterer clusterer) {
		this.clusterer = clusterer;
	}

}
