package zeligB.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.JFileChooser;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zeligB.util.clusteringEvaluators.CalinskiHarabaszIndex;
import zeligB.util.clusteringEvaluators.DomIndex;
import zeligB.util.clusteringEvaluators.DunnIndex;
import zeligB.util.clusteringEvaluators.FiltraExtensoes;
import zeligB.util.clusteringEvaluators.JaccardCoefficient;

public class ClusteringEvaluator3 {

	private static File[] files;
	private static Instances original;

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		String base, saidaArquivo = "";

		System.out.print("Informe o nome da base: ");
		base = sc.nextLine();

		String abordagem;
		boolean mA = true;

		do {
			saidaArquivo = "Dunn,CH,Dom,Jaccard\n";

			System.out.print("Informe a abordagem: ");
			abordagem = sc.nextLine();

			for (int g = 2; g <= 10; g++) {
				System.out.println("Carregando as bases para " + g
						+ " grupos...");
				saidaArquivo += geraResultados(base);
			}

			System.out.print("Salvando em arquivo... ");

			File file = new File("resultados_" + base + "_" + abordagem
					+ ".txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(saidaArquivo);
			bw.close();

			System.out.print("Salvo.\n");

			System.out.println("===========================================");
			System.out.println(saidaArquivo);
			System.out.println("===========================================");

			System.out.print("Adicionar mais uma abordagem (S/N)? ");
			mA = (sc.nextLine().toUpperCase().equals("S")) ? true : false;

		} while (mA);

		sc.close();
	}

	public static String geraResultados(String basename) throws IOException {
		// Scanner sc = new Scanner(System.in);
		// System.out.print("Informe a base: ");
		loadOriginalBase(basename);
		// System.out.println("Base\tDunn\tCH\tDOM\tJaccard");

		AbrirTodos();
		Instances base;

		String saida = "";

		System.out.print("Calculando Índices... ");

		for (int i = 0; i < files.length; i++) {
			FileInputStream inFile = new FileInputStream(files[i]);
			InputStreamReader in = new InputStreamReader(inFile);

			base = new Instances(in);
			base.setClassIndex(base.numAttributes() - 1);

			// System.out.print(base.relationName() + "\t");
			
			saida += with3DecimalPlaces(DunnIndex.calculate(base)) + ",";
			saida += with3DecimalPlaces(CalinskiHarabaszIndex.calculate(base)) + ",";
			saida += with3DecimalPlaces(DomIndex.calculate(original, base)) + ",";
			saida += with3DecimalPlaces(JaccardCoefficient.calculate(original, base)) + "\n";
		}

		System.out.print("Calculados\n");

		return saida;
	}
	
	public static String with3DecimalPlaces(double value){
		if((value >= Double.MIN_VALUE) && (value <= Double.MAX_VALUE)){
			return String.format(Locale.ENGLISH, "%.3f",value);	
		}
		return "0";
	}

	private static void AbrirTodos() {
		String url = ".";

		JFileChooser chooser = null;
		chooser = new JFileChooser(url);
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setMultiSelectionEnabled(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			files = chooser.getSelectedFiles();
		}
	}

	private static void loadOriginalBase(String baseName) {
		try {
			DataSource source = new DataSource(baseName + ".arff");
			original = source.getDataSet();
			original.setClassIndex(original.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
