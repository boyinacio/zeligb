package zeligB.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zeligB.util.clusteringEvaluators.CalinskiHarabaszIndex;
import zeligB.util.clusteringEvaluators.DomIndex;
import zeligB.util.clusteringEvaluators.DunnIndex;
import zeligB.util.clusteringEvaluators.FiltraExtensoes;
import zeligB.util.clusteringEvaluators.JaccardCoefficient;

public class ClusteringEvaluator2 {

	private static File[] files;
	private static Instances original;

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		String base, saidaArquivo = "";
		boolean maisAbordagens, maisGrupos;
		maisAbordagens = maisGrupos = true;

		System.out.print("Informe o nome da base: ");
		base = sc.nextLine();

		String abordagem, mG, mA;
		int nGrupos;
		ArrayList<Integer> grupos;
		ArrayList<double[]> indices;

		saidaArquivo = base + "\n";

		do {
			grupos = new ArrayList<Integer>();
			indices = new ArrayList<double[]>();

			System.out.print("Informe a abordagem: ");
			abordagem = sc.nextLine();

			do {
				System.out.println("Informe a quantidade de grupos: ");
				nGrupos = sc.nextInt(); sc.nextLine();
				grupos.add(nGrupos);

				System.out.println("Carregando as bases...");

				double[] resultados = geraResultados(base);

				indices.add(resultados);

				System.out
						.print("Desejar adicionar mais uma quantidade de grupos [S/N]? ");
				mG = sc.nextLine();
				maisGrupos = (mG.equals("S")) ? true : false;

			} while (maisGrupos);

			System.out.print("Escrevendo saída... ");

			saidaArquivo += abordagem + "\t";

			for (Integer grupo : grupos) {
				saidaArquivo += grupo + "\t";
			}

			saidaArquivo += "\n\t";

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < indices.size(); j++) {
					saidaArquivo += indices.get(j)[i] + "\t";
				}
				saidaArquivo += "\n\t";
			}

			saidaArquivo += "\n";

			System.out.print("Escrita.\n");

			System.out.print("Desejar adicionar mais uma abordagem [S/N]? ");
			mA = sc.nextLine();
			maisAbordagens = (mA.equals("S")) ? true : false;

		} while (maisAbordagens);
				
		System.out.print("Salvando em arquivo... ");

		File file = new File("resultados_"+base+".txt");

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(saidaArquivo);
		bw.close();
					
		System.out.print("Salvo.\n");
		
		System.out.println("===========================================");
		System.out.println(saidaArquivo);
		System.out.println("===========================================");

		sc.close();
	}

	public static double[] geraResultados(String basename) throws IOException {
		// Scanner sc = new Scanner(System.in);
		// System.out.print("Informe a base: ");
		 loadOriginalBase(basename);
		// System.out.println("Base\tDunn\tCH\tDOM\tJaccard");

		AbrirTodos();
		Instances base;

		System.out.print("Calculando Índices... ");

		double[] indices = new double[4];
		
		for (int i = 0; i < indices.length; i++) {
			indices[i] = 0;
		}
				
		double nBases = files.length;
		
		for (int i = 0; i < files.length; i++) {
			FileInputStream inFile = new FileInputStream(files[i]);
			InputStreamReader in = new InputStreamReader(inFile);

			base = new Instances(in);
			base.setClassIndex(base.numAttributes() - 1);

			// System.out.print(base.relationName() + "\t");

			indices[0] = indices[0] + DunnIndex.calculate(base);
			indices[1] = indices[1] + CalinskiHarabaszIndex.calculate(base);
			indices[2] = indices[2] + DomIndex.calculate(original, base);
			indices[3] = indices[3] + JaccardCoefficient.calculate(original, base);
			//
			// System.out.print(dunn + "\t" + ch + "\t");
			// System.out.print(dom + "\t" + jaccard + "\n");

		}

		for (int i = 0; i < indices.length; i++) {
			indices[i] = indices[i] / nBases;
		}

		System.out.print("Calculados.\n");

		return indices;

		// sc.close();

	}

	private static void AbrirTodos() {
		String url = ".";

		JFileChooser chooser = null;
		chooser = new JFileChooser(url);
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setMultiSelectionEnabled(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			files = chooser.getSelectedFiles();
		}
	}

	private static void loadOriginalBase(String baseName) {
		try {
			DataSource source = new DataSource(baseName + ".arff");
			original = source.getDataSet();
			original.setClassIndex(original.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
