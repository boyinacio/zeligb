package zeligB.test;

import java.text.DecimalFormat;
import java.util.Locale;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Test {

	Instances dataset;

	private Instances loadDataset(String filename) {
		Instances base = null;
		try {
			DataSource source = new DataSource(filename);
			base = source.getDataSet();
			base.setClassIndex(base.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		return new Instances(base);
	}

	private boolean equalsInstances(Instance instA, Instance instB) {
		int numAttribs = instA.numAttributes() - 1;

		for (int i = 0; i < numAttribs; i++) {
			if (instA.value(i) != instB.value(i)) {
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {
//		Test t = new Test();
		
		System.out.println(String.format(Locale.ENGLISH, "%.3f",2.4567890));
		
//		Instances dataA = t
//				.loadDataset("results/ronda_policial_dbscan_eps_0.05_minPts_3_10k.arff");
//		Instances dataB = t
//				.loadDataset("results/ronda_policial_dbscan_eps_0.05_minPts_3_12k.arff");

//		Instances dataA = t
//				.loadDataset("datasets/iris.arff");
//		Instances dataB = t
//				.loadDataset("datasets/iris.arff");

		// System.out.println(Math.log(0));

//		System.out.println(DomIndex.calculate(dataA, dataB));

		// for (int i = 0; i <= 10; i++) {
		// for (int j = 0; j <= i; j++) {
		// System.out.print(Mathematics.combinationOf(i, j) + " ");
		// }
		// System.out.println();
		// }

		// int x = 1, y = 3;
		//
		// double z = (double) x / y;
		//
		// System.out.println(z);

	}

}
