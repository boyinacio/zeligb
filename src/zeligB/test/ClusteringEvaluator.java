package zeligB.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.swing.JFileChooser;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zeligB.util.clusteringEvaluators.CalinskiHarabaszIndex;
import zeligB.util.clusteringEvaluators.DomIndex;
import zeligB.util.clusteringEvaluators.DunnIndex;
import zeligB.util.clusteringEvaluators.FiltraExtensoes;
import zeligB.util.clusteringEvaluators.JaccardCoefficient;

public class ClusteringEvaluator {

	private static File[] files;
	private static Instances original;

	/**
	 * a
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe a base: ");
		loadOriginalBase(sc.nextLine());
		AbrirTodos();
		System.out.println("Base\tDunn\tCH\tDOM\tJaccard");

		for (int i = 0; i < files.length; i++) {
			FileInputStream inFile = new FileInputStream(files[i]);
			InputStreamReader in = new InputStreamReader(inFile);

			Instances base = new Instances(in);
			base.setClassIndex(base.numAttributes() - 1);

			System.out.print(base.relationName() + "\t");
						
			double dunn = DunnIndex.calculate(base);
			double ch = CalinskiHarabaszIndex.calculate(base);
			double dom = DomIndex.calculate(original, base);
			double jaccard = JaccardCoefficient.calculate(original, base);

			System.out.print(dunn + "\t" + ch + "\t");
			System.out.print(dom + "\t" + jaccard + "\n");

		}
		
		sc.close();

	}

	private static void AbrirTodos() {
		String url = ".";

		JFileChooser chooser = null;
		chooser = new JFileChooser(url);
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setMultiSelectionEnabled(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			files = chooser.getSelectedFiles();
		}
	}

	private static void loadOriginalBase(String baseName) {
		try {
			DataSource source = new DataSource("bases/" + baseName + ".arff");
			original = source.getDataSet();
			original.setClassIndex(original.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
